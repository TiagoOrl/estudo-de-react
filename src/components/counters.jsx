

import React, { Component } from 'react';
import Counter from './counter';

class Counters extends Component {

    state = {
        counters: [
            {id: 1, value: 3},
            {id: 2, value: 2},
            {id: 3, value: 0},
            {id: 4, value: 0},
        ]
    }

    handleIncrement = (counter) => {
        console.log("handleINcremento")
    }

    handleDelete = (counterId) => {

        const newCounters = this.state.counters.filter(counter => counter.id != counterId); // filtrando a nova lista 
        this.setState({ counters: newCounters });  // atualizando a view com a nova lista
    }

    render () {
        return (
            <div>
                { this.state.counters.map(counter => 
                <Counter 
                    key={counter.id} 
                    onDelete={this.handleDelete} 
                    onIncrement={this.handleIncrement} 
                    
                    counter={counter}
                /> 
                )}
            </div>
        );
    }
}


export default Counters;