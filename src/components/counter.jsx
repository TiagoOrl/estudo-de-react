import React, { Component } from 'react';


class Counter extends Component {

    state ={
        counter: this.props.counter.value
    };


    mudarEstilo() {
        let classes = "badge ";
        this.props.counter.value === 0 ? classes += "badge-warning" : classes += "badge-primary";

        return classes;
    }

    

    render() {

        return (
        <div className= "m-1">

            <span className={this.mudarEstilo()}> {this.props.counter.value} </span>

            <button className="m-2" onClick={() => this.props.onIncrement(this.props.counter)}> Adicionar </button>
            <button className="btn btn-danger btn-sm m-2" onClick={() => this.props.onDelete(this.props.counter.id)}> Delete </button>

        </div>
        )
    }
}

export default Counter;